
#include <stdio.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iopengl *ogl;
local Ikeyboard *kb;
local Imapdata *md;

typedef struct Graphic
{
	int id;
	int width;
	int height;
	
	int rows;
	int cols;
} Graphic;

Graphic g1;
Graphic g2;
Graphic g3;
Graphic g4;
Graphic g5;
Graphic g6;
Graphic g7;

int tx=0;
int ty=0;
int tz=100;
int s=100;
double gz=1.0;
double gr=1.0;

local void DrawPlayerShip(Player *p)
{
	if(!p) return;
	if(p->ship == SHIP_SPEC) return;
	
	int rot=p->Ship.rotation;
	int row=1;
	row+=p->ship*4;
	row+=rot/10;
	int col=1;
	col+=rot % 10;
	ogl->DrawSprite(g6.id,p->Ship.x,p->Ship.y,32,32,col,row,g6.cols,g6.rows);
	
// printf("drawing ship %d rot %d: col=%d row=%d\n",p->ship,rot,col,row);
}

local void DrawTextAt(int x, int y, ChatType type, int sizex, int sizey, char* text)
{
	//fast color conversion
	int color=0;
	switch(type)
	{
		case CHAT_GREY:
			color=0;
		break;
		case CHAT_ARENA:
		case CHAT_PRIV:
		case CHAT_RPRIV:
			color=1;
		break;
		case CHAT_PUBM:
		case CHAT_PUB:
			color=2;
		break;
		case CHAT_MOD:
			color=3;
		break;
		case CHAT_TEAM:
		case CHAT_FREQ:
			color=4;
		break;
		case CHAT_PURPLE:
			color=5;
		break;
		case CHAT_CHAT:
			color=6;
		break;
		case CHAT_PINK:
			color=7;
		break;
		default:
			color=8;
	}

	int dx=x;
	char *c=text;
	while(*c)
	{
		if(*c < ' ' || *c > '~') *c=' '; //no illegal chars
		
		int row=color*2+1;
		if(*c >= 'P') row++;
		int col=*c - ' ' + 1;
		if(col > 48) col-=48;
		
//printf("dw letter: '%c' c=%i z=%i x=%i y=%i col=%i row=%i \n",c,*c,(int)color,dx,y,col,row);
		ogl->DrawSprite(g7.id,dx,y,sizex,sizey,col,row,g7.cols,g7.rows);
		dx+=sizex;
		c++;
	}
}

int boost=10;
local bool menutoggle=false;
void ButtonPressed(Button key, bool down)
{
// printf("key pressed n=%d=%02x\n",key,key);
// printf("bp: %i %02x\n",down,key);

	// BUTTON_PGUP,
	// BUTTON_PGDN,
	// BUTTON_AFTERBURN,
	
	if(key == BUTTON_AFTERBURN)
	{
		int boost=10;
		if(down) boost=50;
	}
	
	if(!down) return;
	
	if(key == BUTTON_MENU_MOD) menutoggle=!menutoggle;
	
	if(key == BUTTON_UP) ty-=boost;
	if(key == BUTTON_DOWN) ty+=boost;
	if(key == BUTTON_LEFT) tx-=boost;
	if(key == BUTTON_RIGHT) tx+=boost;
	
	
	if(key == BUTTON_PGUP) gz-=.025;
	if(key == BUTTON_PGDN) gz+=.025;
	if(key == KB_F1) gr--;
	if(key == KB_F2) gr++;
	if(key == KB_F3) tz--;
	if(key == KB_F4) tz++;
	
	if(tx < 0) tx=0;
	if(tx > 1024*16) tx=1024*16;
	if(ty < 0) ty=0;
	if(ty > 1024*16) ty=1024*16;
	if(gz < 0) gz=0;
}

ticks_t last=0;
int frame=0;
local void DrawGraphics(drawlayer layer)
{
	switch(layer)
	{
		case DL_TILES:
		{
//			char *keystates=kb->KeyboardState();
//			if(keystates[KB_SHIFT]) boost=50;
//			if(keystates[KB_UP]) ty-=boost;
//			if(keystates[KB_DOWN]) ty+=boost;
//			if(keystates[KB_LEFT]) tx-=boost;
//			if(keystates[KB_RIGHT]) tx+=boost;
//			if(keystates[KB_PGUP]) gz-=.025;
//			if(keystates[KB_PGDN]) gz+=.025;
//			if(keystates[KB_F1]) gr--;
//			if(keystates[KB_F2]) gr++;
//			if(keystates[KB_F3]) tz--;
//			if(keystates[KB_F4]) tz++;
//			
			if(tx < 0) tx=0;
			if(tx > 1024*16) tx=1024*16;
			if(ty < 0) ty=0;
			if(ty > 1024*16) ty=1024*16;
			if(gz < 0) gz=0;
			
			ogl->Translate(-tx,-ty);
			ogl->Rotate(gr,tx,ty);
			ogl->Scale(gz);
			
			int x, y;
			for(y=-1; y <= 1024; y++)
				for(x=-1; x <= 1024; x++)
				{
					if(x==-1 || y==-1 || x==1024 || y==1024) ogl->DrawSprite(g5.id,x*16,y*16,16,16,1,2,g5.cols,g5.rows); //border
					else
					{
						// int tol=50; //draw distance limiter
						// if(((x < tx/16-tol) || (x > tx/16+tol)) && ((x < tx/16-tol) || (x > tx/16+tol))) continue;
					
						int tile=md->GetTile(x,y);
						if(tile && (tile >= TILE_START) && (tile <= TILE_END))
						{
							int rowx=tile % g5.cols;
							if(!rowx) rowx=g5.cols;
							int rowy=(tile / g5.cols)+1;
							ogl->DrawSprite(g5.id,x*16,y*16,16,16,rowx,rowy,g5.cols,g5.rows);
						}
					}
				}
				
		}
		break;
		case DL_CHAT:
		{
			// int y=1500;
			// DrawTextAt(100,y+12*0,CHAT_GREY,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*1,CHAT_ARENA,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*2,CHAT_PUB,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*3,CHAT_MOD,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*4,CHAT_TEAM,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*5,CHAT_PURPLE,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*6,CHAT_CHAT,8,12,"This is a line of text");
			// DrawTextAt(100,y+12*7,CHAT_PINK,8,12,"This is a line of text");
			
			char buf[256];
			ticks_t now=current_ticks();
			sprintf(buf,"Camera X: %i (%i)",tx,tx/16);
			DrawTextAt(25,20,CHAT_MOD,8,12,buf);
			sprintf(buf,"Camera Y: %i (%i)",ty,ty/16);
			DrawTextAt(25,40,CHAT_MOD,8,12,buf);
			sprintf(buf,"Camera zoom: %.2f",gz);
			DrawTextAt(25,60,CHAT_CHAT,8,12,buf);
			sprintf(buf,"Camera rotation: %.2f",gr);
			DrawTextAt(25,80,CHAT_CHAT,8,12,buf);
			sprintf(buf,"cur tick: %i",now);
			DrawTextAt(25,100,CHAT_PUB,8,12,buf);
			DrawTextAt(25,150,CHAT_MOD,16,24,"This text is extremely large");
		}
		break;
		case DL_AFTERSHIPS:
		{
			ogl->DrawSprite(g1.id,200,200,s,s,1,1,g1.cols,g1.rows);
			ogl->DrawSprite(g2.id,200,400,s,s,1,1,g2.cols,g2.rows);
			ogl->DrawSprite(g3.id,400,200,s,s,1,1,1,1);
			ogl->DrawSprite(g4.id,400,400,tz,tz,1,1,1,1);
			ogl->DrawSprite(g5.id,600,200,s,s,1,1,1,1);
			ogl->DrawSprite(g6.id,500,2000,360,1152,1,1,1,1);
			ogl->DrawSprite(g7.id,1000,200,s,s,1,1,1,1);
			
			
			ogl->DrawSprite(g4.id,600,400,s,s,3,10,4,10);
			ogl->DrawSprite(g3.id,800,200,s,s,1,1,10,4);
			ogl->DrawSprite(g1.id,800,400,s,s,3,3,5,5);
			
			
			
			char buf[256];
			ticks_t now=current_ticks();
			if(now > (last+100))
			{
				frame++;
				if(frame > 4) frame=1;
				last=now;
			}
			ogl->DrawSprite(g4.id,1000,1000,50,50,frame,10,4,10);
			//needed for anim: frametime, repeat all or remove when done, frame, row only, rows, cols
			//funcs: add, move, remove
		}
		break;
	}

	
}

local void CreateGraphics(void)
{
	ogl->CreateGraphic("graphics/test.bmp",&(g1.id),&(g1.width),&(g1.height),TRUE);
	ogl->CreateGraphic("graphics/test2.bmp",&(g2.id),&(g2.width),&(g2.height),TRUE);
	ogl->CreateGraphic("graphics/test3.bmp",&(g3.id),&(g3.width),&(g3.height),TRUE);
	ogl->CreateGraphic("graphics/bullets.bmp",&(g4.id),&(g4.width),&(g4.height),TRUE);
	// ogl->CreateGraphic("maps/4trench.lvl",&(g5.id),&(g5.width),&(g5.height),TRUE);
	ogl->CreateGraphic("maps/distension.lvl",&(g5.id),&(g5.width),&(g5.height),TRUE);
	ogl->CreateGraphic("graphics/ships.bmp",&(g6.id),&(g6.width),&(g6.height),TRUE);
	ogl->CreateGraphic("graphics/tallfont2.bmp",&(g7.id),&(g7.width),&(g7.height),TRUE);
	
	g1.rows=1;
	g1.cols=1;
	g2.rows=1;
	g2.cols=1;
	g3.rows=4;
	g3.cols=10;
	g4.rows=10;
	g4.cols=4;
	g5.rows=10;
	g5.cols=19;
	g6.rows=32;
	g6.cols=10;
	g7.rows=16; //letter 12px tall
	g7.cols=48; //letter 8px wide
}

local Igfx gint=
{
	INTERFACE_HEAD_INIT(I_GFX,"gfx"),
	DrawTextAt, DrawPlayerShip
};

EXPORT int MM_graphics(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ogl=mm->GetInterface(I_OPENGL);
		// kb=mm->GetInterface(I_KEYBOARD);
		md=mm->GetInterface(I_MAPDATA);
		if(!lm || !ogl || !md) return MM_FAIL;
		
		mm->RegCallback(CB_CREATEGRAPHICS,CreateGraphics);
		mm->RegCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->RegCallback(CB_BUTTONPRESSED,ButtonPressed);
		
		mm->RegInterface(&gint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&gint)) return MM_FAIL;
		
		mm->UnregCallback(CB_CREATEGRAPHICS,CreateGraphics);
		mm->UnregCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->UnregCallback(CB_BUTTONPRESSED,ButtonPressed);
		
		mm->ReleaseInterface(md);
		// mm->ReleaseInterface(kb);
		mm->ReleaseInterface(ogl);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

