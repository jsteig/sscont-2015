

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Igfx *gfx;
local Ichat *chat;

char keystates[256];
bool menustate=FALSE;
bool chatstate=FALSE;

char typingbuf[1024];
int letters=0;
local int resx=0;
local int resy=0;

local void Resolution(int x, int y)
{
	resx=x;
	resy=y;
}

local void DrawGraphics(drawlayer layer)
{
	switch(layer)
	{
		case DL_CHAT:
			if(letters) gfx->DrawTextAt(10,resy-20,CHAT_PINK,8,12,typingbuf);
		break;
	}
}

local bool updatekeyboard(int key, bool down, bool save)
{
	if(save) keystates[key]=down;
	
	if(!down) return false;
	
	//TODO: need to check key def settings here
	
	if(save)
	{
		//convert windows letter codes
		//the dumbass that wrote windows keyboard handling needs to be shot
		
		//called twice on letters on windows
		// if(key >= 0x30 && key <= 0x39) key+=48-30; //ascii 0=48
		// if(key >= 0x41 && key <= 0x5A) key+=97-41; //ascii a=97
		
		// if(key >= 32 && key <= 127) return false;
		// if(key == KB_ESCAPE) return false;
		// if(key == KB_RETURN) return false;
		// if(key == KB_BACK) return false;
		if(key >= 'A' && key <= 'Z') return false; //A-Z
		if(key >= '0' && key <= '9') return false; //0-9
	}
	else
	{
		if(key == KB_ESCAPE) return false;
		if(key == KB_RETURN) return false;
		if(key == KB_BACK) return false;
	}
// printf("%s %2d x%02x %c\n",(save ? "KEY" : "CHAR"),key,key,key);
	
	if(chatstate && !save && (key >= 32 && key <= 127))
	{
		typingbuf[letters]=key;
		letters++;
	}
	else if(chatstate && letters && (key == KB_BACK))
	{
		letters--;
		typingbuf[letters]=0;
	}
	else if(chatstate && letters && (key == KB_RETURN))
	{
// printf("sending chat: %s\n",typingbuf);
		// if(letters) chat->HandleChat(strdup(typingbuf));
		if(letters) chat->HandleChat(typingbuf);
		letters=0;
		ZeroMemory(typingbuf,sizeof(char)*1024);
		// chatstate=false;
	}
	else if(menustate && (key>='1') && (key<='8'))
	{
printf("kb set ship=%d\n",key-'0');
		DO_CBS(CB_SHIPCHANGE,ShipChangeFunc,(key-'0'));
	}
	else if(menustate && (key=='q')) return TRUE;
	
	if(save)
	switch(key)
	{
		case KB_ESCAPE:
			menustate=!menustate;
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_MENU_MOD,down));
			break;
		
		case KB_SHIFT:
		case KB_LSHIFT:
		case KB_RSHIFT:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_AFTERBURN,down));
			break;
		
		case KB_CONTROL:
		case KB_LCONTROL:
		case KB_RCONTROL:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_BULLET,down));
			break;
		
		case KB_MENU:
		case KB_LMENU:
		case KB_RMENU:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_MINE,down)); //normally big minimap
			break;
		
		case KB_RETURN:
			chatstate=!chatstate;
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_CHAT_MOD,down));
			break;
		case KB_BACK:
			// backspace();
		case KB_TAB:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_BOMB,down));
			break;
		
		case KB_UP:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_UP,down));
			break;
		case KB_DOWN:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_DOWN,down));
			break;
		case KB_LEFT:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_LEFT,down));
			break;
		case KB_RIGHT:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_RIGHT,down));
			break;
		
		case KB_PGUP:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_PGUP,down));
			break;
		case KB_PGDN:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_PGDN,down));
			break;
			
		case KB_END:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_CLOAK,down));
			break;
		case KB_HOME:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_XRADAR,down));
			break;
		case KB_INSERT:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_WARP,down));
			break;
		case KB_DELETE:
			DO_CBS(CB_BUTTONPRESSED,ButtonPressedFunc,(BUTTON_MULTI,down));
			break;
	}

	return FALSE;
}

local char* KeyboardState(void)
{
	return keystates;
}

local Ikeyboard kbint=
{
	INTERFACE_HEAD_INIT(I_KEYBOARD,"keyboard"),
	KeyboardState,
	updatekeyboard
};

EXPORT int MM_keyboard(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		gfx=mm->GetInterface(I_GFX);
		chat=mm->GetInterface(I_CHAT);
		if(!lm || !gfx || !chat) return MM_FAIL;
		
		ZeroMemory(keystates,sizeof(char)*256);
		
		mm->RegCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->RegCallback(CB_RESOLUTION,Resolution);
		
		mm->RegInterface(&kbint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&kbint)) return MM_FAIL;
		
		mm->UnregCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->UnregCallback(CB_RESOLUTION,Resolution);
		
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(gfx);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
