
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;


local int finder(char *dest, int destlen, const char *ar, const char *name)
{
	astrncpy(dest,name,destlen);
	return 0;
}

local void error(const char *err)
{
	Error(EXIT_MODLOAD,"Error in modules.conf: %s",err);
}

int main(int argc, char *argv[])
{
	int code=0;

	srand(current_ticks());

	mm=InitModuleManager();
	RegCModLoader(mm);

	
	
	printf("I <main> Loading modules\n");

	/*
	//this stuff used for loading modules.conf
	//but we don't want that stuff exposed to the user now, because we don't trust them.
	char line[256];
	int ret;
	APPContext *ctx;
	ctx=APPInitContext(finder,error,NULL);
	APPAddFile(ctx,"conf/modules.conf");
	while(APPGetLine(ctx,line,256))
	{
		ret=mm->LoadModule(line);
		if(ret == MM_FAIL) Error(EXIT_MODLOAD,"Error in loading module '%s'",line);
	}
	APPFreeContext(ctx);
	*/
	
	
#define LOAD(line) if(mm->LoadModule(line) == MM_FAIL) Error(EXIT_MODLOAD,"Error in loading module '%s'",line)
#include "modules.inc.h"

	
	lm=mm->GetInterface(I_LOGMAN);
	ml=mm->GetInterface(I_MAINLOOP);

	if(!lm) Error(EXIT_MODLOAD,"<main> Logman module missing");
	if(!ml) Error(EXIT_MODLOAD,"<main> Mainloop module missing");

	lm->Log(L_SYNC|L_INFO,"<main> Running main loop");

	code=ml->RunLoop();

	lm->Log(L_SYNC|L_INFO,"<main> Exited main loop, unloading modules");

	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(ml);

	mm->UnloadAllModules();

	UnregCModLoader();
	DeInitModuleManager(mm);
	
	system("PAUSE");

	return code;
}

