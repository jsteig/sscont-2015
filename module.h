
#ifndef __MODMAN_H
#define __MODMAN_H

#include "util.h"

#define MAX_ID_LEN 128
#define INTERFACE_HEAD_DECL InterfaceHead head
#define INTERFACE_HEAD_INIT(iid,name) { MODMAN_MAGIC, iid, name, 0, 0 }
#define MODMAN_MAGIC 0x46692017

typedef enum
{
	MM_LOAD,
	MM_POSTLOAD,
	MM_PREUNLOAD,
	MM_UNLOAD,
	MM_ATTACH,
	MM_DETACH
} mod_action;


#define MM_OK     0  /**< success */
#define MM_FAIL   1  /**< failure */

typedef struct InterfaceHead
{
	unsigned long magic;
	const char *iid, *name;
	int reserved1, refcount;
} InterfaceHead;

typedef struct mod_args_t
{
	char name[32];
	const char *info;
	void *privdata;
} mod_args_t;

typedef int (*ModuleLoaderFunc)(mod_action action, mod_args_t *args, const char *line);


#define I_MODMAN "modman-1"

typedef struct Imodman
{
	INTERFACE_HEAD_DECL;
	
	int (*LoadModule)(const char *specifier);
	int (*UnloadModule)(const char *name);

	void (*EnumModules)(void (*func)(const char *name, const char *info, void *clos), void *clos);
	
	void (*RegInterface)(void *iface);
	int (*UnregInterface)(void *iface);


	void* (*GetInterface)(const char *id);
	void* (*GetInterfaceByName)(const char *name);
	void (*ReleaseInterface)(void *iface);


	void (*RegCallback)(const char *id, void *func);
	void (*UnregCallback)(const char *id, void *func);

	void (*LookupCallback)(const char *id, LinkedList *res);
	void (*FreeLookupResult)(LinkedList *res);


	void (*RegModuleLoader)(const char *signature, ModuleLoaderFunc func);
	void (*UnregModuleLoader)(const char *signature, ModuleLoaderFunc func);

	const char *(*GetModuleInfo)(const char *modname);
	const char *(*GetModuleLoader)(const char *modname);

	
	
	void (*DoStage)(int stage);
	void (*UnloadAllModules)(void);
	void (*NoMoreModules)(void);
} Imodman;


Imodman* InitModuleManager(void);
void DeInitModuleManager(Imodman *mm);



#define DO_CBS(cb, type, args)        \
{                                            \
	LinkedList _a_lst;                       \
	Link *_a_l;                              \
	mm->LookupCallback(cb, &_a_lst);  \
	for(_a_l = LLGetHead(&_a_lst); _a_l; _a_l = _a_l->next)  \
		((type)_a_l->data) args ;            \
	mm->FreeLookupResult(&_a_lst);           \
}

#endif

