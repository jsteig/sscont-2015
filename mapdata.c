
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;

#include "sparse.c"

#define METADATA_MAGIC 0x6c766c65
#define MAX_CHUNK_SIZE (128*1024)


typedef struct chunk
{
	u32 type;
	u32 size;
	Byte data[1];
} chunk;

struct ELVL;

struct Region
{
	struct ELVL *lvl;
	const char *name;
	HashTable *chunks;
	u32 setmap[8];
	
	int tiles;
	LinkedList rle_data;
};

struct RLEEntry
{
	int x;
	int y;
	int width;
	int height;
};

typedef struct ELVL
{
	sparse_arr tiles;
	sparse_arr rgntiles;
	Region ***rgnsets;
	HashTable *attrs;
	HashTable *regions;
	HashTable *rawchunks;
	int errors;
	int flags;
	pthread_mutex_t mtx;
} ELVL;

typedef struct tile_data_t
{
	u32 x : 12;
	u32 y : 12;
	u32 type : 8;
} tile_data_t;

local int read_chunks(HashTable *chunks, const Byte *d, int len)
{
	u32 buf[2]={ 0,0 };
	const chunk *tc;
	chunk *c;

	while(len >= 8)
	{
		/* first check chunk header */
		tc=(chunk*)d;

		if(tc->size > MAX_CHUNK_SIZE || (tc->size + 8) > len) break;

		/* allocate space for the chunk and copy it in */
		c=amalloc(tc->size + 8);
		memcpy(c,d,tc->size + 8);
		d += tc->size + 8;
		len -= tc->size + 8;

		/* add it to the chunk table */
		buf[0]=c->type;
		HashAdd(chunks,(const char *)buf,c);

		/* skip padding Bytes */
		if((tc->size & 3) != 0)
		{
			int padding=4 - (tc->size & 3);
			d += padding;
			len -= padding;
		}
	}

	return len == 0;
}


local void free_region(Region *rgn)
{
	if(rgn->chunks)
	{
		HashEnum(rgn->chunks,hash_enum_afree,NULL);
		HashFree(rgn->chunks);
		LLEnum(&rgn->rle_data,afree);
		LLEmpty(&rgn->rle_data);
	}
	afree(rgn);
}


local int Contains(Region *rgn, int x, int y)
{
	if(rgn->lvl->rgntiles)
	{
		int i=lookup_sparse(rgn->lvl->rgntiles,x,y);
		return rgn->setmap[i >> 5] & (1 << (i & 31));
	}
	else return FALSE;
}


local int rgn_set_len(Region **rgnset)
{
	int c;
	for(c=0; *rgnset; c++,rgnset++) ;
	return c;
}

local void add_rgn_tile(Region *newrgn, int x, int y)
{
	ELVL *lvl=newrgn->lvl;
	Region **oldset,**newset;
	int i,len;

	oldset=lvl->rgnsets[lookup_sparse(lvl->rgntiles,x,y)];

	len=oldset ? rgn_set_len(oldset) : 0;

	/* try looking up new set */
	for(i=1; i < 256; i++)
	{
		Region **tryset=lvl->rgnsets[i];
		if(!tryset) break;
		if(memcmp(oldset,tryset,len * sizeof(Region *)) == 0 && tryset[len] == newrgn)
		{
			/* we found the correct set */
			insert_sparse(lvl->rgntiles,x,y,i);
			return;
		}
	}

	/* the correct set doesn't exist yet */
	if(i < 256)
	{
		int j,offset=i >> 5;
		u32 mask=1 << (i & 31);

		newset=amalloc((len + 2) * sizeof(Region *));
		for(j=0; j < len; j++) (newset[j]=oldset[j])->setmap[offset] |= mask;
		(newset[len]=newrgn)->setmap[offset] |= mask;
		newset[len+1]=NULL;
		lvl->rgnsets[i]=newset;
		insert_sparse(lvl->rgntiles,x,y,i);
	}
	else lm->Log(L_WARN,"<mapdata> overlap limit reached. some region data will be lost.");
}

local int read_rle_tile_data(Region *rgn, const Byte *d, int len)
{
	int cx=0,cy=0,i=0,b,op,d1,n,x,y;

	int last_row=-1;
	LinkedList last_row_data;
	Link *link;

	LLInit(&last_row_data);

	while(i < len)
	{
		if(cx < 0 || cx > 1023 || cy < 0 || cy > 1023) return FALSE;

		b=d[i++];
		op=(b & 192) >> 6;
		d1=b & 31;
		n=d1 + 1;

		if(b & 32) n=(d1 << 8) + d[i++] + 1;

		switch(op)
		{
			case 0:
				/* n empty in a row */
				cx += n;
				break;
			case 1:
				/* n present in a row */
				if(cy != last_row)
				{
					LLEmpty(&last_row_data);
					last_row=cy;
				}
				struct RLEEntry *entry=amalloc(sizeof(*entry));
				entry->x=cx;
				entry->y=cy;
				entry->width=n;
				entry->height=1;
				LLAddLast(&last_row_data,entry);
				LLAddLast(&rgn->rle_data,entry);
				rgn->tiles += n;

				for(x=cx; x < (cx + n); x++) add_rgn_tile(rgn,x,cy);
				cx += n;
				break;
			case 2:
				/* n rows of empty */
				if(cx != 0) return FALSE;
				cy += n;
				break;
			case 3:
				/* repeat last row n times */
				if(cx != 0 || cy == 0) return FALSE;

				for(link=LLGetHead(&last_row_data); link; link=link->next)
				{
					struct RLEEntry *entry=link->data;
					entry->height += n;
					rgn->tiles += n * entry->width;
				}

				for(y=cy; y < (cy + n); y++)
					for(x=0; x < 1024; x++)
						if(Contains(rgn,x,cy - 1)) add_rgn_tile(rgn,x,y);
				cy += n;
				last_row=cy - 1;
				break;
		}

		if(cx == 1024)
		{
			cx=0;
			cy++;
		}
	}

	if(i != len || cy != 1024) return FALSE;

	return TRUE;
}


local void read_plain_tile_data(ELVL *lvl, void *vd, unsigned int len)
{
// printf("lvl read_plain_tile_data(%p,%p,%u)\n",lvl,vd,len);// len=0;
	tile_data_t *td=vd;

	int i=0;
	while(len >= sizeof(tile_data_t))
	{
// printf("lvl tile: x=%4d y=%4d type=%4d\n",td[i].x,td[i].y,td[i].type);
		if(td[i].x < 1024 && td[i].y < 1024)
		{
			if(td[i].type == TILE_TURF_FLAG) lvl->flags++;
			if(td[i].type < TILE_BIG_ASTEROID) insert_sparse(lvl->tiles,td[i].x,td[i].y,td[i].type);
			else
			{
// printf("lvl tile: x=%4d y=%4d type=%4d\n",td[i].x,td[i].y,td[i].type);
				int size=1;
				if(td[i].type == TILE_BIG_ASTEROID) size=2;
				else if(td[i].type == TILE_STATION) size=6;
				else if(td[i].type == TILE_WORMHOLE) size=5;
				
				int x, y;
				for(x=0; x < size; x++)
					for(y=0; y < size; y++)
						insert_sparse(lvl->tiles,td[i].x+x,td[i].y+y,td[i].type);
			}
		}
		else lvl->errors++;

		i++;
		len -= sizeof(tile_data_t);
	}
printf("lvl errors=%d\n",lvl->errors);
}

local int process_region_chunk(const char *key, void *vchunk, void *vrgn)
{
	Region *rgn=vrgn;
	chunk *chunk=vchunk;

	if(chunk->type == MAKE_CHUNK_TYPE(rNAM))
	{
		if(!rgn->name)
		{
			char *name=amalloc(chunk->size + 1);
			memcpy(name,chunk->data,chunk->size);
			rgn->name=name;
		}
		afree(chunk);
		return TRUE;
	}
	else if(chunk->type == MAKE_CHUNK_TYPE(rTIL))
	{
		if(!rgn->lvl->rgntiles) rgn->lvl->rgntiles=init_sparse();
		if(!rgn->lvl->rgnsets) rgn->lvl->rgnsets=amalloc(256 * sizeof(Region **));
		if(!read_rle_tile_data(rgn,chunk->data,chunk->size)) lm->Log(L_WARN,"<mapdata> error in lvl file while reading rle tile data");
		afree(chunk);
		return TRUE;
	}
	else
		return FALSE;
}

local int process_map_chunk(const char *key, void *vchunk, void *vlvl)
{
	ELVL *lvl=vlvl;
	chunk *chunk=vchunk;

	if(chunk->type == MAKE_CHUNK_TYPE(ATTR))
	{
		char keybuf[64],*val;
		const Byte *t;
		t=(Byte *)delimcpy(keybuf,(char *)chunk->data,sizeof(keybuf),'=');
		if(t)
		{
			int vlen=chunk->size - (t - chunk->data);
			val=amalloc(vlen+1);
			memcpy(val,t,vlen);
			if(!lvl->attrs) lvl->attrs=HashAlloc();
			HashAdd(lvl->attrs,keybuf,val);
		}
		afree(chunk);
		return TRUE;
	}
	else if(chunk->type == MAKE_CHUNK_TYPE(REGN))
	{
		Region *rgn=amalloc(sizeof(*rgn));
		rgn->lvl=lvl;
		rgn->chunks=HashAlloc();
		rgn->tiles=0;
		LLInit(&rgn->rle_data);
		if(!read_chunks(rgn->chunks,chunk->data,chunk->size)) lm->Log(L_WARN,"<mapdata> error in lvl while reading region chunks");
		HashEnum(rgn->chunks,process_region_chunk,rgn);

		if(!lvl->regions) lvl->regions=HashAlloc();
		if(rgn->name)
		{
			/* make the region name point to its key in the hash table,
			 * and free the memory used by the temporary name. */
			const char *t=rgn->name;
			rgn->name=HashAdd(lvl->regions,rgn->name,rgn);
			afree(t);
		}
		else free_region(rgn); /* all regions must have a name */

		afree(chunk);
		return TRUE;
	}
	else if(chunk->type == MAKE_CHUNK_TYPE(TSET))
	{
		/* we don't use this,free it to save some memory. */
		afree(chunk);
		return TRUE;
	}
	else if(chunk->type == MAKE_CHUNK_TYPE(TILE))
	{
		read_plain_tile_data(lvl,chunk->data,chunk->size);
		afree(chunk);
		return TRUE;
	}
	else return FALSE; /* keep any other chunks */
}


local int load_from_file(ELVL *lvl, const char *lvlname)
{
// printf("lvl load_from_file(%p,%s)\n",lvl,lvlname);
	struct metadata_header_t
	{
		u32 magic;
		u32 totalsize;
		u32 res1;
	} *mdhead;

	MMapData *map=MapFile(lvlname,FALSE);
	if(!map) return FALSE;

	lvl->tiles=init_sparse();

	Byte *d=map->data;
	BMPFH *bmfh=(BMPFH*)d;
	if(map->len < sizeof(BMPFH)) return FALSE;
	if(bmfh->magic != 19778) return FALSE;
	
// printf("lvl1\n");
	if(bmfh->unused1 != 0)
	{
// printf("lvl2\n");
		/* possible metadata,try to read it */
		mdhead=(struct metadata_header_t*)(d + bmfh->unused1);
		if((map->len >= bmfh->unused1+sizeof(BMPFH)) && 
			(mdhead->magic == METADATA_MAGIC) && 
			(map->len >= bmfh->unused1 + mdhead->totalsize))
		{
// printf("lvl3\n");
			/* looks good. start reading chunks. */
			lvl->rawchunks=HashAlloc();
			if(!read_chunks(lvl->rawchunks,d+bmfh->unused1+12,mdhead->totalsize-12))
				lm->Log(L_WARN,"<mapdata> error in lvl while reading metadata chunks");
			/* turn some of them into more useful data. */
			HashEnum(lvl->rawchunks,process_map_chunk,lvl);
		}
	}
// printf("lvl size=%u\n",(long)bmfh->size);
	d += bmfh->size; //get in position for tile data
	

// printf("lvl ptd len=%d d=%p dt=%p diff=%u)\n",map->len,d,map->data,(u32)d-(u32)(map->data));
	/* now d points to the tile data. read until eof. */
	// read_plain_tile_data(lvl,d,map->len - (d - (const Byte *)map->data));
	// read_plain_tile_data(lvl,d,(u32)(map->data) - (u32)d + map->len);
	// read_plain_tile_data(lvl,d,(u32)d - (u32)(map->data) + map->len);
	read_plain_tile_data(lvl,d,map->len - ((u32)d - (u32)(map->data)));
	// read_plain_tile_data(lvl,d,map->len);

	UnmapFile(map);
	return TRUE;
}


local int hash_enum_free_region(const char *key, void *rgn, void *clos)
{
	free_region(rgn);
	return FALSE;
}

local void clear_level(ELVL *lvl)
{
	if(lvl->tiles)
	{
		delete_sparse(lvl->tiles);
		lvl->tiles=NULL;
	}
	if(lvl->rgntiles)
	{
		delete_sparse(lvl->rgntiles);
		lvl->rgntiles=NULL;
	}
	if(lvl->rgnsets)
	{
		int i;
		for(i=0; i < 256; i++) afree(lvl->rgnsets[i]);
		afree(lvl->rgnsets);
		lvl->rgnsets=NULL;
	}
	if(lvl->attrs)
	{
		HashEnum(lvl->attrs,hash_enum_afree,NULL);
		HashFree(lvl->attrs);
		lvl->attrs=NULL;
	}
	if(lvl->regions)
	{
		HashEnum(lvl->regions,hash_enum_free_region,NULL);
		HashFree(lvl->regions);
		lvl->regions=NULL;
	}
	if(lvl->rawchunks)
	{
		HashEnum(lvl->rawchunks,hash_enum_afree,NULL);
		HashFree(lvl->rawchunks);
		lvl->rawchunks=NULL;
	}
}



local const char * RegionName(Region *rgn)
{
	return rgn->name;
}


local int RegionChunk(Region *rgn,u32 ctype, const void **datap, int *sizep)
{
	if(rgn->chunks)
	{
		chunk *c;

		u32 buf[2]={ 0,0 };
		buf[0]=ctype;
		c=HashGetOne(rgn->chunks,(const char *)buf);
		if(c)
		{
			if(datap) *datap=c->data;
			if(sizep) *sizep=c->size;
			return TRUE;
		}
	}
	return FALSE;
}


struct enum_all_clos
{
	void *clos;
	void (*cb)(void *clos,Region *rgn);
};

local int enum_all_work(const char *rname, void *rgn, void *clos)
{
	struct enum_all_clos *eac=clos;
	eac->cb(eac->clos,rgn);
	return FALSE;
}


ELVL glvl;
local void aaction_work(void)
{
ELVL *lvl=&glvl;
	// pthread_mutex_lock(&lvl->mtx);
	clear_level(lvl);
	
		char mapname[256];
		strcpy(mapname,"maps/distension.lvl");
		// strcpy(mapname,"maps/4trench.lvl");
		// if(GetMapFilename(arena,mapname,sizeof(mapname),NULL) && load_from_file(lvl,mapname))
		if(load_from_file(lvl,mapname))
		// if(load_from_file(lvl,"maps/4trench.lvl"))
		{
			lm->Log(L_INFO,"<mapdata> successfully processed map file '%s'",mapname);
			int i=0;
			// insert_sparse(glvl.tiles,512,512,TILE_SAFE);
			i=lookup_sparse(glvl.tiles,512,512);
			lm->Log(L_INFO,"<mapdata> 512,512=%i",i);
			i=lookup_sparse(glvl.tiles,511,511);
			lm->Log(L_INFO,"<mapdata> 511,511=%i",i);
			i=lookup_sparse(glvl.tiles,513,513);
			lm->Log(L_INFO,"<mapdata> 513,513=%i",i);
			i=lookup_sparse(glvl.tiles,512,837);
			lm->Log(L_INFO,"<mapdata> 512,837=%i",i);
			i=lookup_sparse(glvl.tiles,279,745);
			lm->Log(L_INFO,"<mapdata> 279,745=%i",i);
		}
		else
		{
			/* fall back to emergency. this matches the compressed map in mapnewsdl.c. */
			lm->Log(L_WARN,"<mapdata> error finding or reading level file");
			lvl->tiles=init_sparse();
			lvl->flags=lvl->errors=0;
		}
		
	// pthread_mutex_unlock(&lvl->mtx);
}

local tiletype GetTile(int x, int y)
{
	return lookup_sparse(glvl.tiles,x,y);
}

local Imapdata mapdataint=
{
	INTERFACE_HEAD_INIT(I_MAPDATA,"mapdata"),
	GetTile
};


EXPORT int MM_mapdata(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ml=mm->GetInterface(I_MAINLOOP);
		if(!ml || !lm ) return MM_FAIL;

		mm->RegInterface(&mapdataint);
		
		aaction_work();
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&mapdataint)) return MM_FAIL;

		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);

		return MM_OK;
	}
	return MM_FAIL;
}

