
#ifndef __MAINLOOP_H
#define __MAINLOOP_H


/** hopefully useful exit codes */
typedef enum
{
	EXIT_OK=1, /**< an exit from *shutdown */
	EXIT_GENERAL=2, /**< a general 'something went wrong' error */
	EXIT_MEMORY=3, /**< we ran out of memory */
	EXIT_MODCONF=4, /**< the initial module file is missing */
	EXIT_MODLOAD=5, /**< an error loading initial modules */
	EXIT_CHROOT=6  /**< can't chroot or setuid */
} ml_code;


typedef int (*TimerFunc)(void *param);
typedef void (*CleanupFunc)(void *param);
typedef void (*WorkFunc)(void *param);


#define CB_MAINLOOP "mainloop"
typedef void (*MainLoopFunc)(void);

#define I_MAINLOOP "mainloop-1"

typedef struct Imainloop
{
	INTERFACE_HEAD_DECL;

	void (*SetTimer)(TimerFunc func, int initialdelay, int interval, void *param, void *key);
	void (*ClearTimer)(TimerFunc func, void *key);
	void (*CleanupTimer)(TimerFunc func, void *key, CleanupFunc cleanup);
	
	int (*RunLoop)(void);
	void (*Quit)(ml_code code);
	void (*RunInThread)(WorkFunc func, void *param);
	
} Imainloop;


#endif

