
#ifndef __MAPDATA_H
#define __MAPDATA_H

typedef enum
{
	TILE_NONE          =0,
	TILE_START         =1,
	TILE_BORDER        =20,
	TILE_END           =160,
	TILE_V_DOOR_START  =162,
	TILE_V_DOOR_END    =165,
	TILE_H_DOOR_START  =166,
	TILE_H_DOOR_END    =169,
	TILE_TURF_FLAG     =170,
	TILE_SAFE          =171,
	TILE_GOAL          =172,
	TILE_OVER_START    =173,
	TILE_OVER_END      =175,
	TILE_UNDER_START   =176,
	TILE_UNDER_END     =190,
	TILE_TINY_ASTEROID =216,
	TILE_BIG_ASTEROID  =217,
	TILE_TINY_ASTEROID2=218,
	TILE_STATION       =219,
	TILE_WORMHOLE      =220,
	TILE_BRICK         =250,
} tiletype;

typedef struct Region Region;

struct mapdata_memory_stats_t
{
	int lvlbytes, lvlblocks, rgnbytes, rgnblocks;
	int reserved[8];
};



#define MAKE_CHUNK_TYPE(s) (*(u32*)#s)

#define RCT_ISBASE			MAKE_CHUNK_TYPE(rBSE)
#define RCT_NOANTIWARP		MAKE_CHUNK_TYPE(rNAW)
#define RCT_NOWEAPONS		MAKE_CHUNK_TYPE(rNWP)
#define RCT_NOFLAGS			MAKE_CHUNK_TYPE(rNFL)
#define RCT_NORECVANTI		MAKE_CHUNK_TYPE(rNRA)
#define RCT_NORECVWEPS		MAKE_CHUNK_TYPE(rNRW)


#define I_MAPDATA "mapdata-1"

typedef struct Imapdata
{
	INTERFACE_HEAD_DECL;

	tiletype (*GetTile)(int x, int y);
	
} Imapdata;





#endif

