
#ifndef __CHAT_H
#define __CHAT_H


typedef enum
{
	CHAT_ARENA=0,
	CHAT_PUBM=1,
	CHAT_PUB=2,
	CHAT_TEAM=3,
	CHAT_FREQ=4,
	CHAT_PRIV=5,
	CHAT_MOD=6,
	CHAT_RPRIV=7,
	CHAT_CHAT=9,
	CHAT_GREY=33,
	CHAT_PINK=44,
	CHAT_PURPLE=79
} ChatType;

// #define CB_CHATRECIEVED "chatrecieved"
// typedef void (*ChatRecievedFunc)(int type, char* text);


#define I_CHAT "chat-1"

typedef struct Ichat
{
	INTERFACE_HEAD_DECL;

	void (*SendChat)(ChatType color, int sound, int pid, char* text);
	void (*RecieveChat)(int color, char* text);
	void (*HandleChat)(char* text);
	
} Ichat;


#endif

